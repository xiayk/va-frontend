export function ApplePay() {
	this.isAndroid = uni.getSystemInfoSync().platform == 'android'
	this.pay = (ids, success, error, notAndroidBack) => {
		console.log(2.6)
		if (this.isAndroid){
			notAndroidBack()
			return
		}
		let productid = ids.map(String)
		vm.iapChannel.requestOrder(productid, event => {
			iosPay(event[0].productid, success, error)
		})
	}
	
		console.log(1)
	if(this.isAndroid){
		return
	}
	console.log(2)
	let vm = this
	plus.payment.getChannels(function(channels) {
		console.log(JSON.stringify(channels), 'channels')
		for (var i in channels) {
			var channel = channels[i]
			// 获取 id 为 'appleiap' 的 channel
			if (channel.id === 'appleiap') {
				console.log(2.5)
				vm.iapChannel = channel
			}
		}
	}, function(e) {
		console.log('获取iap支付通道失败：' + e.message)
		throw `获取iap支付通道失败： + ${e.message}`
	})
}
// ApplePay.pay(() => {
	
// }, () => {}, () => {})

function iosPay(id, successFun, errorFun) {
	uni.showLoading({
		title: '支付中请勿离开',
		mask: true
	})
	uni.requestPayment({
		provider: 'appleiap',
		orderInfo: { productid: id, },
		success(res) {
			uni.hideLoading()
			if(successFun){
				successFun()
				return
			}
			uni.showToast({
				title: '充值成功',
				icon: 'none'
			})
			// 后续的操作，看自己的需求
			// 如刷新页面
			// 和后端确认是否支付成功
		},
		fail(err) {
			uni.hideLoading()
			if(errorFun){
				errorFun()
				return
			}
			uni.showToast({
				title: '支付已取消',
				icon: 'none'
			})
			console.log(err)
		},
		complete() {
			uni.hideLoading()
		}
	})
}